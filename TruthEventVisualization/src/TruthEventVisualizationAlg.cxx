// TruthEventVisualization includes
#include "TruthEventVisualizationAlg.h"

//#include "xAODEventInfo/EventInfo.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "GeneratorObjects/McEventCollection.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenParticle.h"

#include <fstream>

#include <TH2.h>
#include <TStyle.h>

#include <TROOT.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLorentzVector.h>
#include <TSystem.h>
#include <TImage.h>
#include <TAttImage.h>
#include <TDatabasePDG.h>
TDatabasePDG *pdg  = 0;

struct SimVertex
{
   TLorentzVector X;
   Int_t DecayVtx;
   std::vector<HepMC::GenParticle*> in;
   std::vector<HepMC::GenParticle*> out;
};




TruthEventVisualizationAlg::TruthEventVisualizationAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


TruthEventVisualizationAlg::~TruthEventVisualizationAlg() {}


StatusCode TruthEventVisualizationAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  if (!pdg) pdg = new TDatabasePDG();

  return StatusCode::SUCCESS;
}

StatusCode TruthEventVisualizationAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode TruthEventVisualizationAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
   
   const EventInfo* ei = 0;
   CHECK( evtStore()->retrieve( ei , "McEventInfo" ) );
   EventID* eID = ei->event_ID();
   TString mode = TString(gSystem->Getenv("mode"));
   int particlePdgCode = TString(gSystem->Getenv("particlePdgCode")).Atoi();
   char fl = TString(gSystem->Getenv("flavour")).Data()[0];
   bool vert = TString(gSystem->Getenv("vert")).Atoi();
   int entry = eID->event_number();
   ATH_MSG_INFO("m_event_number : " << entry );
   ATH_MSG_INFO("mode : " << mode );
   ATH_MSG_INFO("particlePdgCode : " << particlePdgCode );
   ATH_MSG_INFO("flavour : " << fl );
   ATH_MSG_INFO("vert : " << vert );
   //
   TString col;
   char Fl;
   if (fl=='u') { Fl = 'K'; col = "blue"; }
   if (fl=='d') { Fl = 'B'; col = "red"; }
   if (fl=='s') { Fl = 'K'; col = "blue"; }
   if (fl=='b') { Fl = 'B'; col = "red"; }
   if (fl=='c') { Fl = 'D'; col = "green"; }
   if (fl=='t') { Fl = 'B'; col = "red"; }
   //
   const McEventCollection* mcCollection;
   if(evtStore()->retrieve(mcCollection,"GEN_EVENT")==StatusCode::SUCCESS) {
      McEventCollection::const_iterator currentGenEventIter = mcCollection->begin();
      if(currentGenEventIter != mcCollection->end()) {
	 if (mode=="event") {
	    //
	    TString fname(Form("TruthEvent.event%d_all.dot",(int)entry));
	    std::ofstream ofs (fname.Data(), std::ofstream::out);
	    ofs << "digraph event_No"<< entry <<" {" << std::endl;
	    HepMC::GenEvent::particle_const_iterator currentGenParticleIter;
	    int endVtx = 0;
	    for(currentGenParticleIter=(*currentGenEventIter)->particles_begin(); currentGenParticleIter!=(*currentGenEventIter)->particles_end(); ++currentGenParticleIter) {
	       int m_prodVtx = (*currentGenParticleIter)->production_vertex() ? (*currentGenParticleIter)->production_vertex()->barcode() : 0;
	       int m_endVtx = (*currentGenParticleIter)->end_vertex() ? (*currentGenParticleIter)->end_vertex()->barcode() : (endVtx++);
	       int pdgCode = (*currentGenParticleIter) ? (*currentGenParticleIter)->pdg_id() : 0;
	       TParticlePDG * rPDG = pdg->GetParticle(pdgCode);
	       std::string pname(rPDG?rPDG->GetName():"X");
	       ofs << m_prodVtx
		 << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
	       ofs << m_endVtx
		 << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
	       if ( rPDG && ( rPDG->GetName()[0] == fl || rPDG->GetName()[0] == Fl )) {
		  ofs << m_prodVtx << " -> "
		    << m_endVtx << " [color=" << col.Data() << ",penwidth=3.0,label=\""
		    << pname << "\"];" << std::endl;
	       }
	       else {
		  ofs << m_prodVtx << " -> "
		    << m_endVtx << " [label=\""
		    << pname << "\"];" << std::endl;
	       }
	    }
	    ofs << "}" << std::endl;
	    ofs.close();
	    gSystem->Exec(Form("dot -Tpng -O %s",fname.Data()));
	 }
	 //
	 if (mode=="particle") {
	    //
	    std::map<Int_t,HepMC::GenParticle*> zv;
	    HepMC::GenEvent::particle_const_iterator currentGenParticleIter;
	    for(currentGenParticleIter=(*currentGenEventIter)->particles_begin(); currentGenParticleIter!=(*currentGenEventIter)->particles_end(); ++currentGenParticleIter) {
	       int pdgCode = (*currentGenParticleIter)->pdg_id();
	       int barcode = (*currentGenParticleIter)->barcode();
	       if ( !zv[barcode] && pdgCode==particlePdgCode )
		 zv[barcode] = (*currentGenParticleIter);
	    }
	    //
	    for(auto it = zv.begin(); it != zv.end(); ++it) {
	       if ( (*it).second ) {
		  int pdgCode = (*it).second->pdg_id();
		  if (pdgCode==particlePdgCode) {
		     HepMC::GenEvent::particle_const_iterator currentGenParticleIter;
		     for(currentGenParticleIter=(*currentGenEventIter)->particles_begin(); currentGenParticleIter!=(*currentGenEventIter)->particles_end(); ++currentGenParticleIter) {
			int m_endVtx = (*it).second->end_vertex()->barcode();
			int m_prodVtx = (*currentGenParticleIter)->production_vertex()->barcode();
			int barcode = (*currentGenParticleIter)->barcode();
			if ( !zv[barcode] && ( m_endVtx == m_prodVtx ))
			  zv[barcode] = (*currentGenParticleIter);
		     }
		     for(currentGenParticleIter=(*currentGenEventIter)->particles_begin(); currentGenParticleIter!=(*currentGenEventIter)->particles_end(); ++currentGenParticleIter) {
			int m_endVtx = (*currentGenParticleIter)->end_vertex() ? (*currentGenParticleIter)->end_vertex()->barcode() : 0;
			int m_prodVtx = (*it).second->production_vertex()->barcode();
			int barcode = (*currentGenParticleIter)->barcode();
			if ( !zv[barcode] && ( m_prodVtx == m_endVtx ))
			  zv[barcode] = (*currentGenParticleIter);
		     }      
		  }
	       }
	    }
	    //
	    TString fname(Form("TruthEvent.event%d_particle_%d.dot",(int)entry,particlePdgCode));
	    std::ofstream ofs (fname.Data(), std::ofstream::out);
	    ofs << "digraph event_No"<< entry <<" {" << std::endl;
	    if (!vert) ofs << "rankdir=LR;" << std::endl;
	    int endVtx = 0;
	    for(auto it = zv.begin(); it != zv.end(); ++it) {
	       if ( (*it).second ) {
		  int pdgCode = (*it).second->pdg_id();
		  int m_prodVtx = (*it).second->production_vertex()->barcode();
		  int m_endVtx = (*it).second->end_vertex() ? (*it).second->end_vertex()->barcode() : (endVtx++);
		  TParticlePDG * rPDG = pdg->GetParticle(pdgCode);
		  std::string pname(rPDG?rPDG->GetName():"X");
		  ofs << m_endVtx
		    << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		  ofs << m_prodVtx
		    << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		  if ( rPDG && ( rPDG->GetName()[0] == fl || rPDG->GetName()[0] == Fl )) {
		     Double_t wdth = ( rPDG->GetName()[0] == fl ) ? 2.0 : 5.0 ;
		     ofs << m_prodVtx << " -> "
		       << m_endVtx << " [color=" << col.Data() << ",penwidth=" <<
		       wdth << ",label=\""
		       << pname << "\"];" << std::endl;
		  }
		  else {
		     ofs << m_prodVtx << " -> "
		       << m_endVtx << " [label=\""
		       << pname << "\"];" << std::endl;
		  }
	       }
	    }
	    ofs << "}" << std::endl;
	    ofs.close();
	    gSystem->Exec(Form("dot -Tpng -O %s",fname.Data()));
	 }
	 if (mode=="flavor") {
	    //
	    std::vector<HepMC::GenVertex*> vtx;
	    HepMC::GenEvent::vertex_const_iterator currentGenVertexIter;
	    for(currentGenVertexIter=(*currentGenEventIter)->vertices_begin(); currentGenVertexIter!=(*currentGenEventIter)->vertices_end(); ++currentGenVertexIter) {
	       size_t n = 0;
	       HepMC::GenVertex::particles_in_const_iterator currentGenParticleInIter;
	       for(currentGenParticleInIter=(*currentGenVertexIter)->particles_in_const_begin(); currentGenParticleInIter!=(*currentGenVertexIter)->particles_in_const_end(); ++currentGenParticleInIter) {
		  Int_t pdgcode = (*currentGenParticleInIter)->pdg_id();
		  TParticlePDG *p = pdg->GetParticle(pdgcode);
		  if ( p && ( p->GetName()[0] == fl || p->GetName()[0] == Fl )) n++;
	       }
	       HepMC::GenVertex::particles_out_const_iterator currentGenParticleOutIter;
	       for(currentGenParticleOutIter=(*currentGenVertexIter)->particles_out_const_begin(); currentGenParticleOutIter!=(*currentGenVertexIter)->particles_out_const_end(); ++currentGenParticleOutIter) {
		  Int_t pdgcode = (*currentGenParticleOutIter)->pdg_id();
		  TParticlePDG *p = pdg->GetParticle(pdgcode);
		  if ( p && ( p->GetName()[0] == fl || p->GetName()[0] == Fl )) n++;
	       }
	       if (n) vtx.push_back(*currentGenVertexIter);
	    }
	    std::map<Int_t,Int_t> used;
	    TString fname(Form("TruthEvent.event%d_flavor_%c.dot",(int)entry,fl));
	    std::ofstream ofs (fname.Data(), std::ofstream::out);
	    ofs << "digraph event_No"<< entry <<" {" << std::endl;
	    int endVtx = 0;
	    for(UInt_t i=0;i<vtx.size();i++) {
	       HepMC::GenVertex::particles_in_const_iterator currentGenParticleInIter;
	       for(currentGenParticleInIter=vtx.at(i)->particles_in_const_begin(); currentGenParticleInIter!=vtx.at(i)->particles_in_const_end(); ++currentGenParticleInIter) {
		  int m_prodVtx = (*currentGenParticleInIter)->production_vertex()->barcode();
		  int m_endVtx = (*currentGenParticleInIter)->end_vertex() ? (*currentGenParticleInIter)->end_vertex()->barcode() : (endVtx++);
		  if (!used[(*currentGenParticleInIter)->barcode()]) {
		     used[(*currentGenParticleInIter)->barcode()] = 1;
		     int PDG = (*currentGenParticleInIter)->pdg_id();
		     TParticlePDG * rPDG = pdg->GetParticle(PDG);
		     std::string pname(rPDG?rPDG->GetName():"X");
		     ofs << m_prodVtx
		       << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		     ofs << m_endVtx
		       << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		     if ( rPDG && ( rPDG->GetName()[0] == fl || rPDG->GetName()[0] == Fl )) {
			Double_t wdth = ( rPDG->GetName()[0] == fl ) ? 2.0 : 5.0 ;
			ofs << m_prodVtx << " -> "
			  << m_endVtx << " [color="<<col.Data()<<",penwidth=" <<
			  wdth << ",label=\""
			  << pname << "\"];" << std::endl;
		     }
		     else {
			ofs << m_prodVtx << " -> "
			  << m_endVtx << " [label=\""
			  << pname << "\"];" << std::endl;
		     }
		  }
	       }
	       HepMC::GenVertex::particles_out_const_iterator currentGenParticleOutIter;
	       for(currentGenParticleOutIter=vtx.at(i)->particles_out_const_begin(); currentGenParticleOutIter!=vtx.at(i)->particles_out_const_end(); ++currentGenParticleOutIter) {
		  int m_prodVtx = (*currentGenParticleOutIter)->production_vertex()->barcode();
		  int m_endVtx = (*currentGenParticleOutIter)->end_vertex() ? (*currentGenParticleOutIter)->end_vertex()->barcode() : (endVtx++);
		  if (!used[(*currentGenParticleOutIter)->barcode()]) {
		     used[(*currentGenParticleOutIter)->barcode()] = 1;
		     int PDG = (*currentGenParticleOutIter)->pdg_id();
		     TParticlePDG * rPDG = pdg->GetParticle(PDG);
		     std::string pname(rPDG?rPDG->GetName():"X");
		     ofs << m_prodVtx
		       << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		     ofs << m_endVtx
		       << " [shape=ellipse,height=0.0,width=0.0,fontsize=0];" << std::endl;
		     if ( rPDG && ( rPDG->GetName()[0] == fl || rPDG->GetName()[0] == Fl )) {
			Double_t wdth = ( rPDG->GetName()[0] == fl ) ? 2.0 : 5.0 ;
			ofs << m_prodVtx << " -> "
			  << m_endVtx << " [color="<<col.Data()<<",penwidth=" <<
			  wdth << ",label=\""
			  << pname << "\"];" << std::endl;
		     }
		     else {
			ofs << m_prodVtx << " -> "
			  << m_endVtx << " [label=\""
			  << pname << "\"];" << std::endl;
		     }  
		  }
	       }
	    }
	    ofs << "}" << std::endl;
	    ofs.close();
	    gSystem->Exec(Form("dot -Tpng -O %s",fname.Data()));
	 }
      }
   }

   setFilterPassed(true); //if got here, assume that means algorithm passed
   return StatusCode::SUCCESS;
}

      

StatusCode TruthEventVisualizationAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}
