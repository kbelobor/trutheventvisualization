
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../TruthEventVisualizationAlg.h"

DECLARE_ALGORITHM_FACTORY( TruthEventVisualizationAlg )

DECLARE_FACTORY_ENTRIES( TruthEventVisualization ) 
{
  DECLARE_ALGORITHM( TruthEventVisualizationAlg );
}
